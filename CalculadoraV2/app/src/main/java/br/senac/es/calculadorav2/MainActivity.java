package br.senac.es.calculadorav2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private static final String ZERO = "0";
    private static final String PONTO = ".";

    private TextView display;
    private Button btsoma;
    private Button btsubt;
    private Button btdiv;
    private Button btmult;
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private Button bt4;
    private Button bt5;
    private Button bt6;
    private Button bt7;
    private Button bt8;
    private Button bt9;
    private Button bt0;
    private Button btponto;
    private Button btigual;
    private Button btCE;

    double privalor, segvalor;
    private boolean limpar;
    boolean soma,subt,mult,div;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();

        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });



        btsoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (display == null) {
                    display.setText("");
                } else {
                    privalor = Double.parseDouble(display.getText() + "");
                    soma = true;
                    display.setText(null);
                }
            }
        });

        btsubt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                privalor = Double.parseDouble(display.getText() + "");
                subt = true;
                display.setText(null);
            }
        });

        btmult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                privalor = Double.parseDouble(display.getText() + "");
                mult = true;
                display.setText(null);
            }
        });

        btdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                privalor = Double.parseDouble(display.getText() + "");
                div = true;
                display.setText(null);
            }
        });


        btCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCE();
            }
        });

        btponto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btigual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                segvalor = Double.parseDouble(display.getText() + "");

                if (soma == true) {
                    display.setText(privalor + segvalor + "");
                    soma = false;
                }

                if (subt == true) {
                    display.setText(privalor - segvalor + "");
                    subt = false;
                }

                if (mult == true) {
                    display.setText(privalor * segvalor + "");
                    mult = false;
                }

                if (div == true) {
                    display.setText(privalor / segvalor + "");
                    div = false;
                }
            }
        });


    }


    private void escreverDisplay(View view) {

        Button button = (Button) view;
        String digito = button.getText().toString();
        String valorDisplay = display.getText().toString();

        if (valorDisplay.equals(ZERO) || limpar) {
            limpar = false;
            if (!digito.equals(PONTO)) {
                display.setText(digito);
            } else {
                if (!valorDisplay.contains(PONTO)) {
                    valorDisplay += PONTO;
                    display.setText(valorDisplay);
                }
            }
        } else {

            valorDisplay += digito;
            display.setText(valorDisplay);

        }


    }


    private void ButtonCE() {
        this.display.setText(ZERO);
        this.privalor = 0;
        this.segvalor = 0;
    }

    private void initComponents() {
        display = (TextView) findViewById(R.id.display);
        btsoma = (Button) findViewById(R.id.btsoma);
        btsubt = (Button) findViewById(R.id.btsubt);
        btdiv = (Button) findViewById(R.id.btdiv);
        btmult = (Button) findViewById(R.id.btmult);
        bt0 = (Button) findViewById(R.id.bt0);
        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);
        bt3 = (Button) findViewById(R.id.bt3);
        bt4 = (Button) findViewById(R.id.bt4);
        bt5 = (Button) findViewById(R.id.bt5);
        bt6 = (Button) findViewById(R.id.bt6);
        bt7 = (Button) findViewById(R.id.bt7);
        bt8 = (Button) findViewById(R.id.bt8);
        bt9 = (Button) findViewById(R.id.bt9);
        btponto = (Button) findViewById(R.id.btponto);
        btCE = (Button) findViewById(R.id.btCE);
        btigual = (Button) findViewById(R.id.btigual);
    }


        }



